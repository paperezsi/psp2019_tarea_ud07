package psp07_tarea;

import java.security.*; //JCA
import javax.crypto.*; //JCE
import java.io.*; //ficheros
import java.util.logging.Level;


/**
 * De igual manera a lo visto en el tema, ahora te proponemos un ejercicio que genere
 * una cadena de texto y la deje almacenada en un fichero encriptado, en la raíz del
 * proyecto hayas creado, con el nombre fichero.cifrado.
 * 
 * Para encriptar el fichero, utilizarás el algoritmo Rijndael o AES, con las
 * especificaciones de modo y relleno siguientes: Rijndael/ECB/PKCS5Padding.
 * 
 * La clave, la debes generar de la siguiente forma:
 *  - A partir de un número aleatorio con semilla la cadena del nombre de usuario + password.
 *  - Con una longitud o tamaño 128 bits.
 * 
 * @author Pablo
 */
public class Psp07_tarea {

    
    //Método principal main
    public static void main(String[] args) {
        
        String fichero = "fichero.cifrado.txt";
        SecretKey clave= null;

        //Crear la cadena de texto aleatoria de como máximo 40 caracteres
        String cadenaAleatoria = textoAleatorio(40);
        
        

        try {
            //Crear clave para encriptar y desemcriptar
            clave = crearClave();
            
            //Cifrar y descifrar fichero
            cifrarGuardarCadena(fichero , clave, cadenaAleatoria);
            descifrarFichero(fichero, clave);

        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }

    }


    //Crear clavePrivada
    public static SecretKey crearClave(){
        
        SecretKey clave = null;
        
        try {
            //Crea un objeto para generar la clave usando algoritmo AES
            //Generar clave a partir de número aleatori y semilla nombre usuaro+password
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            SecureRandom secRandom = new SecureRandom(solicitarDatosUsuario().getBytes());
            keyGen.init(128,secRandom);
            clave = keyGen.generateKey();

        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return clave;

    }
    
    
    //Método solicitar datos al usuario
    public static String solicitarDatosUsuario() {

        String nombreUsuario = "";
        String password = "";
        String cadena="";

        try {

            //Solicito datos al usuario creo flujos de entrada con buffer
            //========================================================================
            InputStreamReader flujoNombre = new InputStreamReader(System.in);
            BufferedReader flujoNombreBuffer = new BufferedReader(flujoNombre);
            InputStreamReader flujoPassword = new InputStreamReader(System.in);
            BufferedReader flujoPasswordBuffer = new BufferedReader(flujoPassword);

            //Solicito contraseña al usuario y la recojo
            System.out.print("Introduce tu nombre: ");
            nombreUsuario = flujoNombreBuffer.readLine();

            System.out.print("Introduce tu password: ");
            password = flujoPasswordBuffer.readLine();
            
            cadena = nombreUsuario + password;

        } catch (Exception e) {

        }
        
        return cadena;
    }
    

    //Método para encriptar
    private static void cifrarGuardarCadena(String fichero , SecretKey clave, String cadenaAleatoria) throws Exception {

        //Creo los flujos de salida al fichero
        OutputStream flujoSalida = null;
        int bytesLeidos;

        //Imprimo la clave generada
        //System.out.println("La clave generada es: ");
        //mostrarBytes(clave.getEncoded());
        //System.out.println();

        //Creo el objeto Cipher y lo inicio en modo CIFRADO
        Cipher cipher = Cipher.getInstance("Rijndael/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, clave);
        
        
        //Cifro la cadena de texto
        byte[] bytesCadenaEncriptada = cipher.doFinal(cadenaAleatoria.getBytes());

        //Escribir fichero
        byte[] buffer = new byte[1024];
        byte[] bufferCifrado;

        //Escribo en el fichero los datos encriptadso
        flujoSalida = new FileOutputStream(fichero);
        flujoSalida.write(bytesCadenaEncriptada);

        //Cierra ficheros
        flujoSalida.close();

    }

     
    //Método para desencriptar
    private static void descifrarFichero(String fichero, SecretKey clave) throws Exception{

        //fichero de entrada
        InputStream flujoEntrada= null;
        OutputStream flujoSalida= null;
        
        //Creo el cipher y lo pongo en mode desencriptar y le paso la llave
        Cipher cifrador = Cipher.getInstance("Rijndael/ECB/PKCS5Padding");
        cifrador.init(Cipher.DECRYPT_MODE, clave);

        //Acceso al fichero
        flujoEntrada = new FileInputStream(fichero);
        flujoSalida = new FileOutputStream(fichero);
        
        //Flujos de bytes
        byte[] bufferClaro=null;
        byte[]buffer = new byte[1000];
        
        int finFichero=-1;
        int bytesLeidos = flujoEntrada.read(buffer,0,1000);
        
        //Mientras no llegue al final del fichero
        while (bytesLeidos != finFichero) {
            //Pasa texto cifrado al cifrado y lo secifra asignandolo al bufferClaro
            bufferClaro = cifrador.update(buffer, 0, bytesLeidos);
            
            //Escribo el texto desencriptado en el fichero
            flujoSalida.write(bufferClaro);
            bytesLeidos = flujoEntrada.read(buffer,0,1000);
        }

        //Completa el descifrado
        bufferClaro = cifrador.doFinal();
        
        //Grabo el final del fichero del texto claro, si lo hay
        flujoSalida.write(bufferClaro);
                
        //Cierro flujos
        flujoEntrada.close();
        flujoSalida.close();

        //Muestro el contenido del fichero
        abrirFichero(fichero);
    }

    
    //Método para generar una cadena de texto aleagoria
    public static String textoAleatorio(int n) {

        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

        //Constructor cadena
        StringBuilder constructorCadena = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            //Genero un numero randos entre 0 y el numero de caracteres
            int index = (int) (caracteres.length() * Math.random());

            //Añado el caracter corespondiente al constructor
            constructorCadena.append(caracteres.charAt(index));
        }

        return constructorCadena.toString();
    }
    
    
    //Método que muestra bytes
    public static void mostrarBytes(byte[] buffer) {
        System.out.write(buffer, 0, buffer.length);
    }
    
    
    //Método para abrir un fichero de texto y visualizarlo por consola
    public static void abrirFichero(String fichero) {

        FileReader FlujoLectura = null;
        BufferedReader FlujoLecturaBuffer = null;

        try {
            //Creo los flujos en entrada
            FlujoLectura = new FileReader(new File(fichero));
            FlujoLecturaBuffer = new BufferedReader(FlujoLectura);

            // Lectura del fichero
            
            System.out.println("El contenido del fichero es:");
            String linea;
            while ((linea = FlujoLecturaBuffer.readLine()) != null) {
                System.out.println(linea);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());

        } finally {
            //Cierro los recursos
            try {
                if (null != FlujoLectura) {
                    FlujoLectura.close();
                    FlujoLecturaBuffer.close();
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

}
