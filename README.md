
# Tarea para PSP07.
De igual manera a lo visto en el tema, ahora te proponemos un ejercicio que genere una cadena de texto y la deje almacenada en un fichero encriptado, en la raíz del proyecto hayas creado, con el nombre fichero.cifrado.

Para encriptar el fichero, utilizarás el algoritmo Rijndael o AES, con las especificaciones de modo y relleno siguientes: Rijndael/ECB/PKCS5Padding.

La clave, la debes generar de la siguiente forma:
- A partir de un número aleatorio con semilla la cadena del nombre de usuario + password.
- Con una longitud o tamaño 128 bits.


## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2019_tarea_ud07.git
```

## **Descripción y pruebas del ejercicio 1**

Para la resolución de la tarea, creo un nuevo proyecto Java, con una única clase donde implemento siete métodos para realizar cada una de las acciones necesarias para la resolución del ejecicio. De los seis métodos los más importantes son:

crearClave(): Es el método encargado de crear la clave usando algoritmo AES
En este método es importante destacar que la clave se crear de forma aleatoria partiendo del nombre y la constraseña indicada por el usuario a través del objeto secRandom.

```
    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    SecureRandom secRandom = new SecureRandom(solicitarDatosUsuario().getBytes());
    keyGen.init(128,secRandom);
    clavePrivada = keyGen.generateKey();
```

![paso1.PNG](readme_src/paso1.PNG)


cifrarGuardarCadena(): Es el método para encritar el texto, haciendo uso de un objeto Cipher y el algoritomo Rijndale/ECB/PKCS5Padding. En este caso el cipher funcionara en modo encriptar "Cipher.ENCRYPT_MODE".

```
    //Creo el objeto Cipher y lo inicio en modo CIFRADO
    Cipher cipher = Cipher.getInstance("Rijndael/ECB/PKCS5Padding");
    cipher.init(Cipher.ENCRYPT_MODE, clave);
```

![paso2.PNG](readme_src/paso2.PNG)



descifrarFichero(): Es el método encargado de leer el fichero encriptado y convertirlo a texto claro, para ello esta vez el objeto Cipher para encriptar lo configuro en modo desencriptar. "Cipher.DECRYPT_MODE" y le paso la clave.

```
    //Creo el cipher y lo pongo en mode desencriptar y le paso la llave
     Cipher cifrador = Cipher.getInstance("Rijndael/ECB/PKCS5Padding");
    cifrador.init(Cipher.DECRYPT_MODE, clave);
```

![paso3.PNG](readme_src/paso3.PNG)



## **Realización de pruebas del ejercicio 1**

Para probar el ejecicio simplemento ejecuto el programa en el IDE y compruebo que se generara correctamente el fichero, para posteriormente ver que el resultado que muestra en consola es el correcto.

![paso4.PNG](readme_src/paso4.PNG)


## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)